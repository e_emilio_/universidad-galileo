package org.emanuelgomez.bd;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.DriverManager;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;

public class Conexion {
    
    private static Conexion instancia;
    private Connection conexion;
    private Statement enunciado;

    public Conexion() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }catch(IllegalAccessException e){
            e.printStackTrace();
        }catch(InstantiationException e){
            e.printStackTrace();
        }
        try{
            conexion = DriverManager.getConnection("jdbc:sqlserver://MOJO:0;InstanceName=SQLEXPRESS;databaseName=ControlVentas;user=Administrador;password=123");
            enunciado = conexion.createStatement();
        
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public ResultSet hacerConsulta(String consulta){
        ResultSet resultado = null;
        try{
            resultado = enunciado.executeQuery(consulta);
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    public static Conexion getInstancia(){
        if (instancia == null){
            instancia = new Conexion();
        }
        return instancia;
    }
    
    public void ejecutarSentencia(String sentencia){
        try{
            enunciado.execute(sentencia);
        }catch(SQLException e){
            e.printStackTrace();
        }
        
    }


}
