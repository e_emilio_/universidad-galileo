package org.emanuelgomez.modelo;
import javax.swing.table.AbstractTableModel;
import org.emanuelgomez.manejadores.ManejadorDeProveedor;
import org.emanuelgomez.bean.Proveedor;

public class ModeloDeDatosProveedor extends AbstractTableModel {
    
    private ManejadorDeProveedor manejador = new ManejadorDeProveedor();
    private String[ ] encabezados = {"Codigo de Proveedor","Nit","Razón Social","Direccion",
        "Telefono","PaginaWeb","Email"};
    public ModeloDeDatosProveedor() {
        
    }
    
    public int getColumnCount(){
        return encabezados.length;
    }
    
    public  int getRowCount(){
        return manejador.getListaDeProveedor().size();
    }
    
    public String getColumnName (int columna){
        return encabezados[columna];
    }
    
    public Object getValueAt(int fila, int columna){
        String resultado = "";
        Proveedor proveedores = manejador.getListaDeProveedor().get(fila);
        switch(columna){
            case 0:
                resultado = String.valueOf(proveedores.getCodigoProveedor());
                break;
            case 1:
                resultado = proveedores.getNit();
                break;
            case 2:
                resultado = proveedores.getRazonSocial();
                break;
            case 3:
                resultado = proveedores.getDireccion();
                break;
            case 4:
                resultado = proveedores.getTelefono();
                break; 
            case 5:
                resultado = proveedores.getPaginaWeb();
                break;
            case 6:
                resultado = proveedores.getEmail();
                break;                
        }
        return resultado;
    }                
    
    public void eliminar(int fila){
        manejador.eliminar(manejador.getProveedor(fila));
        fireTableDataChanged();
        
    }
    public void agregar(Proveedor elemento){
        manejador.agregar(elemento);
        fireTableDataChanged();
    }
    
    public void modificar (Proveedor elemento){
        manejador.modificar(elemento);
        fireTableDataChanged();
    }
    
    public Proveedor getCategoria(int fila){
        return manejador.getListaDeProveedor.get(fila);
    }       
}
