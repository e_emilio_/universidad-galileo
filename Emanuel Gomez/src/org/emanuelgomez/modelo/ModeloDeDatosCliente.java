package org.emanuelgomez.modelo;
import javax.swing.table.AbstractTableModel;
import org.emanuelgomez.manejadores.ManejadorDeCliente;
import org.emanuelgomez.bean.Cliente;

public class ModeloDeDatosCliente extends AbstractTableModel {
    
    private ManejadorDeCliente manejador = new ManejadorDeCliente();
    private String[ ] encabezados = {"Nit","Dpi","Direccion","Nombre","Telefono","Email"};
    public ModeloDeDatosCliente() {        
    }
    
    public int getColumnCount(){
        return encabezados.length;
    }
    
    public  int getRowCount(){
        return manejador.getListaDeCliente().size();
    }
    
    public String getColumnName (int columna){
        return encabezados[columna];
    }
    
    public Object getValueAt(int fila, int columna){
        String resultado = "";
        Cliente cliente = manejador.getListaDeCliente().get(fila);
        switch(columna){
            case 0:
                resultado = cliente.getNit();
                break;
            case 1:
                resultado = cliente.getDpi();
                break;
            case 2:
                resultado = cliente.getDireccion();
                break;
            case 3:
                resultado = cliente.getNombre();
                break;
            case 4:
                resultado = cliente.getTelefono();
                break; 
            case 5:
                resultado = cliente.getEmail();
                break;    
        }
        return resultado;
    }   
    
    public void eliminar(int fila){
        manejador.eliminar(manejador.getCliente(fila));
        fireTableDataChanged();
        
    }
    public void agregar(Cliente elemento){
        manejador.agregar(elemento);
        fireTableDataChanged();
    }    

    public void modificar (Cliente elemento){
        manejador.modificar(elemento);
        fireTableDataChanged();
    }
    
    public Cliente getCategoria(int fila){
        return manejador.getListaDeCliente.get(fila);
    }    
}
