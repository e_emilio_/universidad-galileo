package org.emanuelgomez.modelo;
import javax.swing.table.AbstractTableModel;
import org.emanuelgomez.bean.Categoria;
import org.emanuelgomez.manejadores.ManejadorDeCategoria;


public class ModeloDeDatosCategoria extends AbstractTableModel {
    
    private ManejadorDeCategoria manejador = new ManejadorDeCategoria();
    private String[ ] encabezados = {"Código","Descripción"};
    public ModeloDeDatosCategoria() {
        
    }
    
    public int getColumnCount(){
        return encabezados.length;
    }
    
    public  int getRowCount(){
        return manejador.getListaDeCategoria().size();
    }
    
    public String getColumnName (int columna){
        return encabezados[columna];
    }
    
    public Object getValueAt(int fila, int columna){
        String resultado = "";
        Categoria categoria = manejador.getListaDeCategoria().get(fila);
        switch(columna){
            case 0:
                resultado = String.valueOf(categoria.getCodigo());
                break;
            case 1:
                resultado = categoria.getDescripcion();
                break;                
        }
        return resultado;
    }      
    public void eliminar(int fila){
        manejador.eliminar(manejador.getCategoria(fila));
        fireTableDataChanged();        
    }
    public void agregar(Categoria elemento){
        manejador.agregar(elemento);
        fireTableDataChanged();
    }
    
    public void modificar (Categoria elemento){
        manejador.modificar(elemento);
        fireTableDataChanged();
    }
    
    public Categoria getCategoria(int fila){
        return manejador.setListaDeCategoria.get(fila);
    }
}
