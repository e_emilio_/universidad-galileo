package org.emanuelgomez.modelo;
import javax.swing.table.AbstractTableModel;
import org.emanuelgomez.manejadores.ManejadorDeProducto;
import org.emanuelgomez.bean.Producto;

public class ModeloDeDatosProducto extends AbstractTableModel {
    
    private ManejadorDeProducto manejador = new ManejadorDeProducto();
    private String[ ] encabezados = {"Codigo de Producto","Descripcion","Precio por Mayor",
        "Precio por Docena","Precio por Mayor","Tipo de Empaque","Existencia"};
    public ModeloDeDatosProducto() {        
    }
    
    public int getColumnCount(){
        return encabezados.length;
    }
    
    public  int getRowCount(){
        return manejador.getListaDeProducto().size();
    }
    
    public String getColumnName (int columna){
        return encabezados[columna];
    }
    
    public Object getValueAt(int fila, int columna){
        String resultado = "";
        Producto producto = manejador.getListaDeProducto().get(fila);
        switch(columna){
            case 0:
                resultado = String.valueOf(producto.getCodigoProducto());
                break;
            case 1:
                resultado = producto.getDescripcion();
                break;
            case 2:
                resultado = String.valueOf(producto.getPrecioUnitario());
                break;
            case 3:
                resultado = String.valueOf(producto.getPrecioDocena());
                break;
            case 4:
                resultado = String.valueOf(producto.getPrecioPorMayor());
                break; 
            case 5:
                resultado = producto.getTipoEmpaque();
                break;
            case 6:
                resultado = String.valueOf(producto.getExistencia());
                break;    
        }
        return resultado;
    }       
    public void eliminar(int fila){
        manejador.eliminar(manejador.getProducto(fila));
        fireTableDataChanged();
        
    }
    public void agregar(Producto elemento){
        manejador.agregar(elemento);
        fireTableDataChanged();
    } 
    
    public void modificar (Producto elemento){
        manejador.modificar(elemento);
        fireTableDataChanged();
    }
    
    public Producto getCategoria(int fila){
        return manejador.getListaDeProducto.get(fila);
    }    
}
