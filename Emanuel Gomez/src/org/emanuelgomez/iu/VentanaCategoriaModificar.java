package org.emanuelgomez.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.emanuelgomez.iu.CatalogoDialogo;
import org.emanuelgomez.modelo.ModeloDeDatosCategoria;
import org.emanuelgomez.bean.Categoria;

public class VentanaCategoriaModificar extends CatalogoDialogo implements ActionListener{
    
    private JTextField txtCategoria;
    private JLabel lblCategoria;
    private ModeloDeDatosCategoria modelo;
    private Categoria categoria;
    public VentanaCategoriaModificar(String titulo, int ancho, int alto, ModeloDeDatosCategoria modelo, Categoria categoria) {
        super(titulo, ancho, alto);
        this.categoria = categoria;
        this.modelo = modelo;
        super.getContentPane().add(getTxtCategoria());
        super.getContentPane().add(getLblCategoria());
        getBtnGuardar().addActionListener(this);
        getTxtCategoria().setText(categoria.getDescripcion());
        this.setModal(true);
        this.setVisible(true);          
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){   
            categoria.setDescripcion(txtCategoria.getText());
            modelo.modificar(categoria);
            this.dispose();
        }
    }

    public JTextField getTxtCategoria() {
        if(txtCategoria == null){
            txtCategoria = new JTextField();
            txtCategoria.setBounds(140,30,175,25);
        }
        return txtCategoria;
    }

    public void setTxtCategoria(JTextField txtCategoria) {
        this.txtCategoria = txtCategoria;
    }

    public JLabel getLblCategoria() {
        if(lblCategoria == null){
            lblCategoria = new JLabel("Ingrese Dato");
            lblCategoria.setBounds(40,30,140,25);               
        }
        return lblCategoria;        
    }

    public void setLblCategoria(JLabel lblCategoria) {
        this.lblCategoria = lblCategoria;
    }          
}
