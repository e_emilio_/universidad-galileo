package org.emanuelgomez.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.emanuelgomez.iu.CatalogoDialogo;
import org.emanuelgomez.modelo.ModeloDeDatosProveedor;
import org.emanuelgomez.bean.Proveedor;

public class VentanaProveedorModificar extends CatalogoDialogo implements ActionListener{
 
    private JTextField txtProveedor;
    private JLabel lblProveedor;
    private ModeloDeDatosProveedor modelo;
    private Proveedor proveedor;
    public VentanaProveedorModificar(String titulo, int ancho, int alto, ModeloDeDatosProveedor modelo,Proveedor proveedor) {
        super(titulo, ancho, alto);
        this.proveedor = proveedor;
        this.modelo = modelo;        
        super.getContentPane().add(getTxtProveedor());
        super.getContentPane().add(getLblProveedor());
        getBtnGuardar().addActionListener(this);
        getTxtProveedor().setText(proveedor.getNit());        
        this.setModal(true);
        this.setVisible(true);    
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){   
            proveedor.setNit(txtProveedor.getText());
            modelo.modificar(proveedor);
            this.dispose();
        }
    }

    public JTextField getTxtProveedor() {
        if(txtProveedor == null){
            txtProveedor = new JTextField();
            txtProveedor.setBounds(140,30,175,25);
        }
        return txtProveedor;
    }

    public void setTxtProveedor(JTextField txtProveedor) {
        this.txtProveedor = txtProveedor;
    }

    public JLabel getLblProveedor() {
        if(lblProveedor == null){
            lblProveedor = new JLabel("Ingrese Dato");
            lblProveedor.setBounds(40,30,140,25);               
        }
        return lblProveedor;        
    }

    public void setLblProveedor(JLabel lblProveedor) {
        this.lblProveedor = lblProveedor;
    }        
}
