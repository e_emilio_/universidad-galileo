package org.emanuelgomez.iu;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.emanuelgomez.modelo.ModeloDeDatosCategoria;
import org.emanuelgomez.iu.VentanaCategoriaNuevo;
import org.emanuelgomez.iu.VentanaCategoriaModificar;
public class VentanaManejadorCategoria extends Catalogo implements ActionListener{
    
    private ModeloDeDatosCategoria modelo;
    public VentanaManejadorCategoria(String titulo, int ancho, int alto) {
        super(titulo, ancho, alto);
        getTblDatos().setModel(getModelo());
        getBtnEliminar().addActionListener(this);
        getBtnNuevo().addActionListener(this);
        getBtnModificar().addActionListener(this);
    }

    public ModeloDeDatosCategoria getModelo() {
        if(modelo == null){
            modelo = new ModeloDeDatosCategoria();
        }
        return modelo;
    }

    public void setModelo(ModeloDeDatosCategoria modelo) {
        this.modelo = modelo;
    }   
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnEliminar()){
            int respuesta = JOptionPane.showConfirmDialog(null,"Desea eliminar el registro","Eliminar Categoria",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            if(respuesta == 0){
                modelo.eliminar(getTblDatos().getSelectedRow());
            }            
        }else if(e.getSource() == getBtnNuevo()){
                new VentanaCategoriaNuevo("Nueva Categoria",400,200,getModelo());
            }else if(e.getSource() == getBtnModificar()){
                new VentanaCategoriaModificar("Nueva Categoria",400,200,getModelo(),getModelo().getCategoria(getTblDatos().getSelectedRow()));
            }
    }
}
