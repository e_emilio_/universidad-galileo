package org.emanuelgomez.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.emanuelgomez.iu.CatalogoDialogo;
import org.emanuelgomez.modelo.ModeloDeDatosCliente;
import org.emanuelgomez.bean.Cliente;

public class VentanaClienteNuevo extends CatalogoDialogo implements ActionListener{
    
    private JTextField txtCliente;
    private JLabel lblCliente;
    private ModeloDeDatosCliente modelo;
    public VentanaClienteNuevo(String titulo, int ancho, int alto, ModeloDeDatosCliente modelo) {
        super(titulo, ancho, alto);
        super.getContentPane().add(getTxtCliente());
        super.getContentPane().add(getLblCliente());
        getBtnGuardar().addActionListener(this);
        this.setModal(true);
        this.setVisible(true);    
        this.modelo = modelo;
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){            
            modelo.agregar(new Cliente(0,getTxtCliente().getText()));
            this.dispose();
        }
    }

    public JTextField getTxtCliente() {
        if(txtCliente == null){
            txtCliente = new JTextField();
            txtCliente.setBounds(140,30,175,25);
        }
        return txtCliente;
    }

    public void setTxtCliente(JTextField txtCliente) {
        this.txtCliente = txtCliente;
    }

    public JLabel getLblCliente() {
        if(lblCliente == null){
            lblCliente = new JLabel("Ingrese Dato");
            lblCliente.setBounds(40,30,140,25);               
        }
        return lblCliente;        
    }

    public void setLblCliente(JLabel lblCliente) {
        this.lblCliente = lblCliente;
    }        
}
    
