package org.emanuelgomez.iu;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.emanuelgomez.modelo.ModeloDeDatosCategoria;
import org.emanuelgomez.modelo.ModeloDeDatosCliente;

public class VentanaManejadorCliente extends Catalogo implements ActionListener{
    
    private ModeloDeDatosCliente modelo;
    public VentanaManejadorCliente(String titulo, int ancho, int alto){
        super(titulo,ancho,alto);   
        getTblDatos().setModel(getModelo());
        getBtnEliminar().addActionListener(this);
        getBtnNuevo().addActionListener(this);       
    }

    public ModeloDeDatosCliente getModelo() {
        if(modelo == null){
            modelo = new ModeloDeDatosCliente();
        }
        return modelo;
    }

    public void setModelo(ModeloDeDatosCliente modelo) {
        this.modelo = modelo;
    }   
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnEliminar()){
            int respuesta = JOptionPane.showConfirmDialog(null,"Desea eliminar el registro","Eliminar Categoria",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            if(respuesta == 0){
                modelo.eliminar(getTblDatos().getSelectedRow());
            }            
        }else if(e.getSource() == getBtnNuevo()){
                new VentanaClienteNuevo("Nueva Categoria",400,200,getModelo());
            }

    }    
}
