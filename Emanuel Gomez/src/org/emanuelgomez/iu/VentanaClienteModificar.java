package org.emanuelgomez.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.emanuelgomez.iu.CatalogoDialogo;
import org.emanuelgomez.modelo.ModeloDeDatosCliente;
import org.emanuelgomez.bean.Cliente;

public class VentanaClienteModificar extends CatalogoDialogo implements ActionListener{

    private JTextField txtCliente;
    private JLabel lblCliente;
    private ModeloDeDatosCliente modelo;
    private Cliente cliente;
    public VentanaClienteModificar(String titulo, int ancho, int alto, ModeloDeDatosCliente modelo, Cliente cliente) {
        super(titulo, ancho, alto);
        this.cliente = cliente;
        this.modelo = modelo;
        super.getContentPane().add(getTxtCliente());
        super.getContentPane().add(getLblCliente());
        getBtnGuardar().addActionListener(this);
        getTxtCliente().setText(cliente.getDpi());
        this.setModal(true);
        this.setVisible(true);          
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){   
            cliente.setDpi(txtCliente.getText());
            modelo.modificar(cliente);
            this.dispose();
        }
    }

    public JTextField getTxtCliente() {
        if(txtCliente == null){
            txtCliente = new JTextField();
            txtCliente.setBounds(140,30,175,25);
        }
        return txtCliente;
    }

    public void setTxtCliente(JTextField txtCliente) {
        this.txtCliente = txtCliente;
    }

    public JLabel getLblCliente() {
        if(lblCliente == null){
            lblCliente = new JLabel("Ingrese Dato");
            lblCliente.setBounds(40,30,140,25);               
        }
        return lblCliente;        
    }

    public void setLblCliente(JLabel lblCliente) {
        this.lblCliente = lblCliente;
    }          
}    

