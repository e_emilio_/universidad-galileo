package org.emanuelgomez.iu;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JDesktopPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import org.emanuelgomez.iu.VentanaManejadorCategoria;
public class VentanaPrincipal extends JFrame implements ActionListener{
    
    private JDesktopPane panelVentana;
    private JMenuBar menuPrincipal;
    private JMenu login;
    private JMenu catalogo;
    private JMenuItem catalogoCategoria;
    private JMenuItem catalogoCliente;
    private JMenuItem catalogoProducto;
    private JMenuItem catalogoProveedor;
    private JMenuItem iniciarSesion;
    private JMenuItem cerrarSesion;

    public VentanaPrincipal(){
        this.setTitle("Control de ventas versión 1.1");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);
        this.setJMenuBar(getMenuPrincipal());
        panelVentana = new JDesktopPane(){
            public void paintComponent(Graphics g){
                g.drawImage((new ImageIcon(getClass().getResource("/org/emanuelgomez/recursos/imagenes/fondo1.jpg")).getImage()), 0, 0,getWidth(),getHeight(),this);
                setOpaque(false);                
            }
        };
        this.getContentPane().add(panelVentana,BorderLayout.CENTER);
        this.setVisible(true);
    }
       
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == getCatalogoCategoria()){
            new VentanaManejadorCategoria("Categoria",635,430);            
        }
        if(e.getSource() == getCatalogoCliente()){
            new VentanaManejadorCliente("Cliente",635,430);            
        }        
        if(e.getSource() == getCatalogoProducto()){
            new VentanaManejadorProducto("Producto",635,430);            
        } 
        if(e.getSource() == getCatalogoProveedor()){
            new VentanaManejadorProveedor("Proveedor",635,430);            
        } 
    }

    public JDesktopPane getPanelVentana() {
        return panelVentana;
    }

    public void setPanelVentana(JDesktopPane panelVentana) {
        this.panelVentana = panelVentana;
    }

    public JMenu getCatalogo() {
        if(catalogo == null){
            catalogo = new JMenu("Catalogo");
            catalogo.setMnemonic('C');
            catalogo.setToolTipText("Catalogo");
            catalogo.setIcon(new ImageIcon(getClass().getResource("/org/emanuelgomez/recursos/imagenes/catalogo.png")));
            catalogo.add(getCatalogoCategoria());
            catalogo.add(getCatalogoCliente());
            catalogo.add(getCatalogoProducto());
            catalogo.add(getCatalogoProveedor());
        }        
        return catalogo;
    }

    public void setCatalogo(JMenu Catalogo) {            
        this.catalogo = catalogo;
    }

    public JMenuItem getCatalogoCategoria() {
         if(catalogoCategoria == null){
            catalogoCategoria = new JMenuItem("Categoria");
            catalogoCategoria.setMnemonic('T');
            catalogoCategoria.setToolTipText("Categorías");
            catalogoCategoria.setIcon(new ImageIcon(getClass().getResource("/org/emanuelgomez/recursos/imagenes/cliente.png")));
            catalogoCategoria.addActionListener(this);
            catalogoCategoria.setVisible(true);
         }   
        return catalogoCategoria;
    }

    public void setCatalogoCategoria(JMenuItem catalogoCategoria) {
        this.catalogoCategoria = catalogoCategoria;
    }

    public JMenuItem getCatalogoCliente() {
        if(catalogoCliente == null){
            catalogoCliente = new JMenuItem("Cliente");
            catalogoCliente.setMnemonic('E');
            catalogoCliente.setToolTipText("Clientes");
            catalogoCliente.setIcon(new ImageIcon(getClass().getResource("/org/emanuelgomez/recursos/imagenes/categoria.png")));
            catalogoCliente.addActionListener(this);
            catalogoCliente.setVisible(true);
         }  
        return catalogoCliente;
    }

    public void setCatalogoCliente(JMenuItem catalogoCliente) {
        this.catalogoCliente = catalogoCliente;
    }

    public JMenuItem getCatalogoProducto() {
        if(catalogoProducto == null){
            catalogoProducto = new JMenuItem("Producto");
            catalogoProducto.setMnemonic('P');
            catalogoProducto.setToolTipText("Productos");
            catalogoProducto.setIcon(new ImageIcon(getClass().getResource("/org/emanuelgomez/recursos/imagenes/producto.png")));
            catalogoProducto.addActionListener(this);
            catalogoProducto.setVisible(true);
         }  
        return catalogoProducto;
    }

    public void setCatalogoProducto(JMenuItem catalogoProducto) {
        this.catalogoProducto = catalogoProducto;
    }

    public JMenuItem getCatalogoProveedor() {
        if(catalogoProveedor == null){
            catalogoProveedor = new JMenuItem("Prooveedor");
            catalogoProveedor.setMnemonic('R');
            catalogoProveedor.setToolTipText("Proveedores");
            catalogoProveedor.setIcon(new ImageIcon(getClass().getResource("/org/emanuelgomez/recursos/imagenes/proveedor.png")));
            catalogoProveedor.addActionListener(this);
            catalogoProveedor.setVisible(true);
         }          
        
        return catalogoProveedor;
    }

    public void setCatalogoProoveedor(JMenuItem catalogoProoveedor) {
        this.catalogoProveedor = catalogoProoveedor;
    }
    
    
    
    /*--------------------------------*/

    public JMenuBar getMenuPrincipal() {
        if(menuPrincipal == null){
            menuPrincipal = new JMenuBar();
            menuPrincipal.add(getLogin());
            menuPrincipal.add(getCatalogo()); 
        }
        return menuPrincipal;
    }

    public void setMenuPrincipal(JMenuBar menuPrincipal) {
        this.menuPrincipal = menuPrincipal;
    }

    public JMenu getLogin() {
        if(login == null){
            login = new JMenu("Login");
            login.setMnemonic('L');
            login.setToolTipText("Login de Usuario");
            login.setIcon(new ImageIcon(getClass().getResource("/org/emanuelgomez/recursos/imagenes/home.png")));
            login.add(getIniciarSesion());
            login.add(getCerrarSesion());
        }
                  
        return login;
    }

    public void setLogin(JMenu login) {
        this.login = login;
    }

    public JMenuItem getIniciarSesion() {
        if(iniciarSesion == null){
            iniciarSesion = new JMenuItem("Inicion de Sesion");
            iniciarSesion.setMnemonic('I');
            iniciarSesion.setToolTipText("Iniciar sesión de Usuario");
            iniciarSesion.setIcon(new ImageIcon(getClass().getResource("/org/emanuelgomez/recursos/imagenes/abrir.png")));
            iniciarSesion.addActionListener(this);
            iniciarSesion.setVisible(true);
        }
        return iniciarSesion;
    }

    public void setIniciarSesion(JMenuItem iniciarSesion) {
        
        this.iniciarSesion = iniciarSesion;
    }

    public JMenuItem getCerrarSesion() {
        if(cerrarSesion == null){
            cerrarSesion = new JMenuItem("Cerrar  Sesion");
            cerrarSesion.setMnemonic('S');
            cerrarSesion.setToolTipText("Cerrar sesión de Usuario");
            cerrarSesion.setIcon(new ImageIcon(getClass().getResource("/org/emanuelgomez/recursos/imagenes/cerrar.png")));
            cerrarSesion.addActionListener(this);
            cerrarSesion.setVisible(true);
        }        
        return cerrarSesion;
    }

    public void setCerrarSesion(JMenuItem cerrarSesion) {
        this.cerrarSesion = cerrarSesion;
    }
    
    
}
