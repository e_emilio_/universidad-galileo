package org.emanuelgomez.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.emanuelgomez.iu.CatalogoDialogo;
import org.emanuelgomez.modelo.ModeloDeDatosProducto;
import org.emanuelgomez.bean.Producto;


public class VentanaProductoNuevo extends CatalogoDialogo implements ActionListener{
    
    private JTextField txtProducto;
    private JLabel lblProducto;
    private ModeloDeDatosProducto modelo;
    public VentanaProductoNuevo(String titulo, int ancho, int alto, ModeloDeDatosProducto modelo) {
        super(titulo, ancho, alto);
        super.getContentPane().add(getTxtProducto());
        super.getContentPane().add(getLblProducto());
        getBtnGuardar().addActionListener(this);
        this.setModal(true);
        this.setVisible(true);    
        this.modelo = modelo;
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnGuardar()){
            
            modelo.agregar(new Producto (0,getTxtProducto().getText()));
            this.dispose();
        }
    }

    public JTextField getTxtProducto() {
        if(txtProducto == null){
            txtProducto = new JTextField();
            txtProducto.setBounds(140,30,175,25);
        }
        return txtProducto;
    }

    public void setTxtProducto(JTextField txtProducto) {
        this.txtProducto = txtProducto;
    }

    public JLabel getLblProducto() {
        if(lblProducto == null){
            lblProducto = new JLabel("Ingrese Dato");
            lblProducto.setBounds(40,30,140,25);               
        }
        return lblProducto;        
    }

    public void setLblProducto(JLabel lblCategoria) {
        this.lblProducto = lblProducto;
    }        
}

