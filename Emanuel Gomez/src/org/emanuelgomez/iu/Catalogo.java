package org.emanuelgomez.iu;
import javax.swing.JTable;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JButton;


public class Catalogo extends JFrame{
    private String titulo;
    private int ancho;
    private int alto;
    private JTable tblDatos;
    private JScrollPane scrTableDatos;
    private JLabel lblTitulo;
    private JLabel lblBarra;
    private JButton btnNuevo;
    private JButton btnEliminar;
    private JButton btnModificar;
    private JButton btnReporte;
    
    public Catalogo(String titulo, int ancho, int alto){
        this.titulo = titulo;
        this.ancho = ancho;
        this.alto = alto;
        this.setLayout(null);
        this.setTitle(titulo);
        this.setSize(ancho,alto);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setVisible(true);
        getContentPane().add(getLblTitulo());
        getContentPane().add(getScrTableDatos());
        getContentPane().add(getBtnNuevo());
        getContentPane().add(getBtnEliminar());
        getContentPane().add(getBtnModificar());
        getContentPane().add(getBtnReporte());
        }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getAncho() {
        return ancho;
    }

    public void setAncho(int ancho) {
        this.ancho = ancho;
    }

    public int getAlto() {
        return alto;
    }

    public void setAlto(int alto) {
        this.alto = alto;
    }

    public JTable getTblDatos() {
        if(tblDatos == null){
            tblDatos = new JTable();
         }    
        return tblDatos;
    }

    public void setTblDatos(JTable tblDatos) {
        this.tblDatos = tblDatos;
    }

    public JScrollPane getScrTableDatos() {
        if(scrTableDatos == null){
            scrTableDatos = new JScrollPane();
            scrTableDatos.setViewportView(getTblDatos());
            scrTableDatos.setBounds(10,40,600,300); 
        }
        return scrTableDatos;
    }

    public void setScrTableDatos(JScrollPane scrTableDatos) {
        this.scrTableDatos = scrTableDatos;
    }

    public JLabel getLblTitulo() {
        if(lblTitulo == null){
            lblTitulo = new JLabel(getTitulo());
            lblTitulo.setBounds(15,-50,300,150);
        }
        return lblTitulo;
    }

    public void setLblTitulo(JLabel lblTitulo) {
        this.lblTitulo = lblTitulo;
    }

    public JLabel getLblBarra() {
        return lblBarra;
    }

    public void setLblBarra(JLabel lblBarra) {
        this.lblBarra = lblBarra;
    }

    public JButton getBtnNuevo() {
        if(btnNuevo == null){
            btnNuevo = new JButton("Nuevo");
            btnNuevo.setBounds(10,338,120,40);
        }
        return btnNuevo;
    }

    public void setBtnNuevo(JButton btnNuevo) {
        this.btnNuevo = btnNuevo;
    }

    public JButton getBtnEliminar() {
        if(btnEliminar == null){
            btnEliminar = new JButton("Eliminar");
            btnEliminar.setBounds(135,338,120,40);
        }       
        return btnEliminar;
    }

    public void setBtnEliminar(JButton btnEliminar) {
        this.btnEliminar = btnEliminar;
    }

    public JButton getBtnModificar() {
        if(btnModificar == null){
            btnModificar = new JButton("Modificar");
            btnModificar.setBounds(260,338,120,40);
        }        
        return btnModificar;
    }

    public void setBtnModificar(JButton btnModificar) {
        this.btnModificar = btnModificar;
    }

    public JButton getBtnReporte() {
        if(btnReporte == null){
            btnReporte = new JButton("Reporte");
            btnReporte.setBounds(385,338,120,40);
        }         
        return btnReporte;
    }

    public void setBtnReporte(JButton btnReporte) {
        this.btnReporte = btnReporte;
    }
    
    
         
}

