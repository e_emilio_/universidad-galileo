package org.emanuelgomez.iu;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.emanuelgomez.modelo.ModeloDeDatosProveedor;

public class VentanaManejadorProveedor extends Catalogo implements ActionListener{
    
    private ModeloDeDatosProveedor modelo;
    public VentanaManejadorProveedor(String titulo, int ancho, int alto){
    super(titulo,ancho,alto);   
    getTblDatos().setModel(getModelo());
    getBtnEliminar().addActionListener(this);
        getBtnNuevo().addActionListener(this);
    }

    public ModeloDeDatosProveedor getModelo() {
        if(modelo == null){
            modelo = new ModeloDeDatosProveedor();
        }        
        return modelo;
    }

    public void setModelo(ModeloDeDatosProveedor modelo) {
        this.modelo = modelo;
    }
    
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == getBtnEliminar()){
            int respuesta = JOptionPane.showConfirmDialog(null,"Desea eliminar el registro","Eliminar Categoria",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            if(respuesta == 0){
                modelo.eliminar(getTblDatos().getSelectedRow());
            }            
        }else if(e.getSource() == getBtnNuevo()){
                new VentanaProveedorNuevo("Nueva Categoria",400,200,getModelo());
            }
    }        
}
