package org.emanuelgomez.iu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JButton;
import javax.swing.JLabel;

public class CatalogoDialogo extends JDialog{
    private String titulo;
    private int ancho;
    private int alto;
    private JButton btnGuardar;
    private JButton btnCancelar;
    public CatalogoDialogo(String titulo, int ancho, int alto) {
        this.titulo = titulo;
        this.ancho = ancho;
        this.alto = alto;        
        this.setSize(ancho,alto);
        this.setTitle("Categoria" + titulo);
        this.setLayout(null);
        this.getContentPane().add(getBtnGuardar());
        this.getContentPane().add(getBtnCancelar());     
        
    }

    public JButton getBtnGuardar() {
        if(btnGuardar == null){
            btnGuardar = new JButton("Guardar");
            btnGuardar.setBounds(165,115,90,25);
        }        
        return btnGuardar;
    }

    public void setBtnGuardar(JButton btnGuardar) {
        this.btnGuardar = btnGuardar;
    }

    public JButton getBtnCancelar() {
        if(btnCancelar == null){
            btnCancelar = new JButton("Cancelar");
            btnCancelar.setBounds(275,115,90,25);
        }            
        return btnCancelar;
    }

    public void setBtnCancelar(JButton btnCancelar) {
        this.btnCancelar = btnCancelar;
    }        
}











