package org.emanuelgomez.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.emanuelgomez.bean.Cliente;
import org.emanuelgomez.bd.Conexion;

public class ManejadorDeCliente {
 
private ArrayList<Cliente> listaDeCliente = new ArrayList<Cliente>();

      public ManejadorDeCliente (){ 
          getDatosCliente();
      }
      
      public void getDatosCliente(){
          
          try{
              ResultSet resultado = Conexion.getInstancia().hacerConsulta("execute sp_ConsultaCliente");
              while(resultado.next()){
              listaDeCliente.add(
                      new Cliente(resultado.getString("nit"),resultado.getString("dpi"),resultado.getString("direccion"),
                              resultado.getString("nombre"),resultado.getString("telefono"),resultado.getString("email")));
              }              
          }catch(SQLException e){
              e.printStackTrace();
          }
      }
	  public void agregar(Cliente elemento){ 
              Conexion.getInstancia().ejecutarSentencia("execute sp_InsertarCliente" + elemento.getDpi());
	      listaDeCliente.removeAll(listaDeCliente);
              getDatosCliente();
      }
	  public void eliminar(Cliente elemento){ 
            listaDeCliente.remove(elemento);
	  }
          
          public void modificar (Cliente elemento){
              Conexion.getInstancia().ejecutarSentencia("execute sp_ModificarCategoria" +
                    elemento.getNit() + ",‘" + elemento.getDpi() + "’" + ",‘" + elemento.getDireccion() + "’" + ",‘" + elemento.getNombre() + "’"
              + ",‘" + elemento.getTelefono() + "’" + ",‘" + elemento.getEmail() + "’");
          }          
	  public Cliente buscar(String nit){ 
	        Cliente resultado = null;
	        for(Cliente elemento : listaDeCliente){ 
               if(elemento.getNit() == nit){ 
               resultado = elemento;
			   break;  
		       }
	        }
            return resultado;			
      }
	  public Cliente getElemento(int elemento){ 
      return listaDeCliente.get(elemento);
	  }
	  public ArrayList<Cliente> getListaDeCliente(){ 
            return listaDeCliente;
	  }
	  public void setListaDeCliente(ArrayList<Cliente> listaDeCliente){ 
      this.listaDeCliente = listaDeCliente;
	  }    
    
}
