package org.emanuelgomez.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.emanuelgomez.bean.Categoria;
import org.emanuelgomez.bd.Conexion;

public class ManejadorDeCategoria {

private ArrayList<Categoria> listaDeCategoria = new ArrayList<Categoria>();

      public ManejadorDeCategoria (){ 
          getDatosCategoria();
      }
      
      public void getDatosCategoria(){         
          try{
              ResultSet resultado = Conexion.getInstancia().hacerConsulta("execute sp_ConsultaCategoria");
              while(resultado.next()){
              listaDeCategoria.add(new Categoria(resultado.getInt("codigo"),resultado.getString("descripcion")));
              }             
          }catch(SQLException e){
              e.printStackTrace();
          }
      }
	  public void agregar(Categoria elemento){ 
              Conexion.getInstancia().ejecutarSentencia("execute sp_InsertarCategoria" + elemento.getDescripcion());
	      listaDeCategoria.removeAll(listaDeCategoria);
              getDatosCategoria();
      }
	  public void eliminar(Categoria elemento){ 
            Conexion.getInstancia().ejecutarSentencia("execute sp_EliminarCategoria" + elemento.getCodigo());  
            listaDeCategoria.remove(elemento);
	  }
          
          public void modificar (Categoria elemento){
              Conexion.getInstancia().ejecutarSentencia("execute sp_ModificarCategoria" +
                    elemento.getCodigo() + ",‘" + elemento.getDescripcion() + "’");
          }
          
	  public Categoria buscar(int codigo){ 
	        Categoria resultado = null;
	        for(Categoria elemento : listaDeCategoria){ 
               if(elemento.getCodigo() == codigo){ 
               resultado = elemento;
			   break;  
		       }
	        }
            return resultado;			
      }
	  public Categoria getElemento(int elemento){ 
      return listaDeCategoria.get(elemento);
	  }
	  public ArrayList<Categoria> getListaDeCategoria(){ 
            return listaDeCategoria;
	  }
	  public void setListaDeCategoria(ArrayList<Categoria> listaDeCategoria){ 
      this.listaDeCategoria = listaDeCategoria;
	  }    
}
