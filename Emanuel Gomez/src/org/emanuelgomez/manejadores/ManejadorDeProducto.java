package org.emanuelgomez.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.emanuelgomez.bean.Producto;
import org.emanuelgomez.bd.Conexion;

public class ManejadorDeProducto {
 
private ArrayList<Producto> listaDeProducto = new ArrayList<Producto>();

      public ManejadorDeProducto (){ 
          getDatosProducto();
      }
      
      public void getDatosProducto(){
          
          try{
              ResultSet resultado = Conexion.getInstancia().hacerConsulta("execute sp_ConsultaProducto");
              while(resultado.next()){
              listaDeProducto.add(
                      new Producto(resultado.getInt("codigoProducto"),resultado.getString("descripcion"),resultado.getFloat("precioUnitario"),
                              resultado.getFloat("precioDocena"),resultado.getFloat("precioPorMayor"),resultado.getString("tipoEmpaque"),
                                   resultado.getInt("existencia")));
              }              
          }catch(SQLException e){
              e.printStackTrace();
          }
      }
	  public void agregar(Producto elemento){ 
              Conexion.getInstancia().ejecutarSentencia("execute sp_InsertarCategoria" + elemento.getDescripcion());
	      listaDeProducto.removeAll(listaDeProducto);
              getDatosProducto();
      }
	  public void eliminar(Producto elemento){ 
            listaDeProducto.remove(elemento);
	  }
          
          public void modificar (Producto elemento){
              Conexion.getInstancia().ejecutarSentencia("execute sp_ModificarCategoria" +
                    elemento.getCodigoProducto() + ",‘" + elemento.getDescripcion() + "’" + elemento.getPrecioUnitario() + elemento.getPrecioDocena() + elemento.getPrecioPorMayor() + "’"
              + ",‘" + elemento.getTipoEmpaque() + "’" + ",‘" + elemento.getExistencia() + "’");
          }
          
	  public Producto buscar(int codigoProducto){ 
	        Producto resultado = null;
	        for(Producto elemento : listaDeProducto){ 
               if(elemento.getCodigoProducto() == codigoProducto){ 
               resultado = elemento;
			   break;  
		       }
	        }
            return resultado;			
      }
	  public Producto getElemento(int elemento){ 
      return listaDeProducto.get(elemento);
	  }
	  public ArrayList<Producto> getListaDeProducto(){ 
            return listaDeProducto;
	  }
	  public void setListaDeProducto(ArrayList<Producto> listaDeProducto){ 
      this.listaDeProducto = listaDeProducto;
	  }
    
}
