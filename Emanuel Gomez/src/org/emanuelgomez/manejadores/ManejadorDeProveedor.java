package org.emanuelgomez.manejadores;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.emanuelgomez.bean.Proveedor;
import org.emanuelgomez.bd.Conexion;

public class ManejadorDeProveedor {
 
private ArrayList<Proveedor> listaDeProveedor = new ArrayList<Proveedor>();

      public ManejadorDeProveedor (){ 
          getDatosProveedor();
      }
      
      public void getDatosProveedor(){          
          try{
              ResultSet resultado = Conexion.getInstancia().hacerConsulta("execute sp_ConsultaProveedor");
              while(resultado.next()){
              listaDeProveedor.add(
                      new Proveedor(resultado.getInt("codigoProveedor"),resultado.getString("nit"),resultado.getString("razonSocial"),
                              resultado.getString("direccion"),resultado.getString("telefono"),resultado.getString("email"),
                                   resultado.getString("paginaWeb")));
              }              
          }catch(SQLException e){
              e.printStackTrace();
          }
      }
      	  
      public void agregar(Proveedor elemento){ 
          Conexion.getInstancia().ejecutarSentencia("execute sp_InsertarProveedor" + elemento.getNit());
          listaDeProveedor.removeAll(listaDeProveedor);
          getDatosProveedor();
      }
	 
      public void eliminar(Proveedor elemento){ 
          listaDeProveedor.remove(elemento);
      }
      
       public void modificar (Proveedor elemento){
           Conexion.getInstancia().ejecutarSentencia("execute sp_ModificarProveedor" +
                   elemento.getCodigoProveedor() + ",‘" + elemento.getNit() + "’" + ",‘" +elemento.getRazonSocial() + "’" + ",‘" + elemento.getDireccion() + "’" +  ",’" + elemento.getTelefono() + ",’"
                   + ",‘" + elemento.getPaginaWeb() + "’" + ",‘" + elemento.getEmail() + "’");
       }       
	
      public Proveedor buscar(int codigoProveedor){ 
          Proveedor resultado = null;
          for(Proveedor elemento : listaDeProveedor){ 
              if(elemento.getCodigoProveedor() == codigoProveedor){ 
                  resultado = elemento;
                  break;
              }
	  }
            return resultado;			
      }

      public Proveedor getElemento(int elemento){ 
          return listaDeProveedor.get(elemento);
      }
      
      public ArrayList<Proveedor> getListaDeProveedor(){ 
          return listaDeProveedor;
      }
      
      public void setListaDeProveedor(ArrayList<Proveedor> listaDeProveedor){ 
          this.listaDeProveedor = listaDeProveedor;
      }    
}
