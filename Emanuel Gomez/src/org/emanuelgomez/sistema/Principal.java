package org.emanuelgomez.sistema;
import org.emanuelgomez.iu.Catalogo;
import org.emanuelgomez.iu.VentanaManejadorCategoria;
import org.emanuelgomez.iu.VentanaManejadorCliente;
import org.emanuelgomez.iu.VentanaManejadorProducto;
import org.emanuelgomez.iu.VentanaManejadorProveedor;
import org.emanuelgomez.bd.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.emanuelgomez.iu.VentanaPrincipal;

public class Principal {
    
    public static void main (String args[ ]){ 
    SubstanceLookAndFeel.setSkin("org.jvnet.substance.SubstanceSkin");
    new VentanaPrincipal();
    }        
}
