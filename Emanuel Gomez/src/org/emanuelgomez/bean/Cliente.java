package org.emanuelgomez.bean;

/**
 *
 * @author Emanuel
 */
public class Cliente {

private String nit;
private String dpi;
private String direccion;
private String nombre;
private String telefono;
private String email;


public String getNit(){
return nit;
}
public void setNit (String nit){
this.nit = nit;
}
     public String getDpi(){ 
     return dpi;
     }
     public void setDpi (String dpi){
     this.dpi = dpi;
     }
           public String getDireccion(){
           return direccion;
           }
           public void setDireccion (String direccion){
           this.direccion = direccion;
           }
                 public String getNombre(){ 
                 return nombre;
                 }
                 public void setNombre (String nombre){
                 this.nombre = nombre;
                 }                       
			           public String getTelefono(){
                       return telefono;
                       }
                       public void setTelefono (String telefono){
                       this.telefono = telefono;
                       }
                             public String getEmail(){ 
                             return email;
                             }
                             public void setEmail (String email){
                             this.email = email;
                             } 

    public Cliente (){
    } 

    public Cliente(String nit, String dpi, String direccion, String nombre, String telefono, String email) {
        this.nit = nit;
        this.dpi = dpi;
        this.direccion = direccion;
        this.nombre = nombre;
        this.telefono = telefono;
        this.email = email;
    }

    }
