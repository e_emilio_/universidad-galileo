package org.emanuelgomez.bean;

/**
 *
 * @author Emanuel
 */
public class Categoria {
    
private int codigo;
private String descripcion;


public int getCodigo(){ 
return codigo;
}
public void setCodigo (int codigo){
this.codigo = codigo;
}

     public String getDescripcion(){
     return descripcion;
     }
     public void setDescripcion (String descripcion){
     this.descripcion = descripcion;
     }

                               
    public Categoria (){
    } 

    public Categoria(int codigo, String descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
    }
        			     
}
