package org.emanuelgomez.bean;

/**
 *
 * @author Emanuel
 */
public class Proveedor {
 
private int codigoProveedor;
private String nit;
private String razonSocial;
private String direccion;
private String telefono;
private String paginaWeb;
private String email;



public int getCodigoProveedor(){ 
return codigoProveedor;
}
public void setCodigoProveedor (int codigoProveedor){
this.codigoProveedor = codigoProveedor;
}
    public String getNit(){ 
    return nit;
    }
    public void setNit (String nit){
    this.nit = nit;
    }
       public String getRazonSocial(){
       return razonSocial;
       }
       public void setRazonSocial (String razonSocial){
       this.razonSocial = razonSocial;
       }
          public String getDireccion(){ 
          return direccion;
          }
          public void setDireccion (String direccion){
          this.direccion = direccion;
          }
               public String getTelefono(){
               return telefono;
               }
               public void setTelefono (String telefono){
               this.telefono = telefono;
               }
                     public String getPaginaWeb(){ 
                     return paginaWeb;
                     }
                     public void setPaginaWeb (String paginaWeb){
                     this.paginaWeb = paginaWeb;
                     }                       
			   public String getEmail(){
                           return email;
                           }
                           public void setEmail (String email){
                           this.email = email;
                          }
                                                          
    public Proveedor (){
    }

    public Proveedor(int codigoProveedor, String nit, String razonSocial, String direccion, String telefono, String paginaWeb, String email) {
        this.codigoProveedor = codigoProveedor;
        this.nit = nit;
        this.razonSocial = razonSocial;
        this.direccion = direccion;
        this.telefono = telefono;
        this.paginaWeb = paginaWeb;
        this.email = email;
    }
}
