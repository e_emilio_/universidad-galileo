package org.emanuelgomez.bean;

/**
 *
 * @author Emanuel
 */
public class Producto {

private int codigoProducto;
private String descripcion;
private float precioUnitario;
private float precioDocena;
private float precioPorMayor;
private String tipoEmpaque;
private int existencia;


public int getCodigoProducto(){ 
return codigoProducto;
}
public void setCodigoProducto (int codigoProducto){
this.codigoProducto = codigoProducto;
}
   public String getDescripcion(){
   return descripcion;
   }
   public void setDescripcion (String Descripcion){
   this.descripcion = descripcion;
   }
      public float getPrecioUnitario(){ 
      return precioUnitario;
      }
      public void setPrecioUnitario (float precioUnitario){
      this.precioUnitario = precioUnitario;
      }
           public float getPrecioDocena(){
           return precioDocena;
           }
           public void setPrecioDocena (float precioDocena){
           this.precioDocena = precioDocena;
           }
                 public float getPrecioPorMayor(){ 
                 return precioPorMayor;
                 }
                 public void setPrecioPorMayor (float precioPorMayor){
                 this.precioPorMayor = precioPorMayor;
                 }                       
			           public String getTipoEmpaque(){
                       return tipoEmpaque;
                       }
                       public void setTipoEmpaque (String tipoEmpaque){
                       this.tipoEmpaque = tipoEmpaque;
                       }
                             public int getExistencia(){ 
                             return existencia;
                             }
                             public void setExistencia (int existencia){
                             this.existencia = existencia;
                             } 

                             
    public Producto (){
    } 

    public Producto(int codigoProducto, String descripcion, float precioUnitario, float precioDocena, float precioPorMayor, String tipoEmpaque, int existencia) {
        this.codigoProducto = codigoProducto;
        this.descripcion = descripcion;
        this.precioUnitario = precioUnitario;
        this.precioDocena = precioDocena;
        this.precioPorMayor = precioPorMayor;
        this.tipoEmpaque = tipoEmpaque;
        this.existencia = existencia;
    }
}
